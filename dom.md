# DOM (Document Object Model)

### Contents:

- Introduction
- What is DOM?
- How does DOM help?
- Accessing the DOM
- JS helper Methods
- Conclusion
- References

## 1. Introduction:

Internet is the most useful tool that modern computers have. In order to access the user side of the internet from any device like computer, mobile phone, etc we need to have special software installed in the system called Browser. Browser is the gateway to the internet. The information on the internet is served to the user in form of a web page which loads into the browser and the user can view and interact with the web page. When a browser loads data from a server, it uses a rendering engine to convert the data into text and images. This rendering engine is different in different browsers but they serve the same purpose by obeying the universal web standards. Some rendering engines include WebKit engine used by google chrome, Gecko used by Firefox, etc. The data which is transferred is written in Hypertext Markup Language and the web browser reads this document and shows the contents in a structured way.

## 2. What is DOM?

DOM stands for Document Object Model. The HTML document which is parsed by the rendering engine of the browser converts the HTML into object representation known as DOM tree or ‘content tree. An HTML document consists of several tags and elements and each HTML tag consists of content that is to be shown in the browser to the user. The browser has its own way of representing the HTML document. This representation is known as DOM. <br>

DOM tree is just like a tree data structure. The nodes in the DOM tree represent the HTML tags/elements. These nodes can have children connected to them. These tree-like structures can sometimes become very complex when the HTML document contains many nested tags. Every browser has a javascript engine (v8 for chrome, spidermonkey for firefox, etc) which parse this DOM tree in order to show the HTML content to the user on the browser.

## 3. How Does it Help?

The main function of DOM in a browser is that DOM acts as an Application Programming Interface (API) for HTML and XML documents. It is a way through which the javascript interacts with HTML and CSS. It defines the HTML tags/elements as an object. Some of the most crucial use of DOM is:

- It is used to represent the logical structure of the document
- It acts as an API for Javascript to manipulate the view of the HTML document
- It helps in defining the properties of the HTML tags/elements.
  <br>
  <br>
  A sample HTML code is shown below:

```
<html>
    <head>
        <title>DOM</title>
    </head>
    <body>
        <h1>This is a header</h1>
        <h2>This is a sub header </h2>
        <div>
             This is a div
           <p>This is a paragraph inside div</p>
            <div>
                <h2>Another div inside div</h2>
             </div>
       </div>
    </body>
</ html>
```

DOM Tree for this HTML page is as given below:

![](./dom.png)

<br>

## 4. How to access DOM?

DOM is nothing but a structured representation of HTML as objects where nodes represent the HTML tags or content. In order to access the DOM content, we need to traverse to the desired node from the root node. The knowledge of CSS is essential to access the DOM content. CSS selectors are used to label the HTML tags and it is one of the most popular ways of accessing the DOM content. The table below shows some of the most popular ways of accessing the DOM content.

| Types             |   Selector |         Method          |
| :---------------- | ---------: | :---------------------: |
| ID                |        #id |    getElementById()     |
| Class             | .classname | getElementByClassName() |
| Tag               |    tagname |  getElementByTagName()  |
| Selector (single) |            |     querySelector()     |
| Selector (All)    |            |   querySelectorAll()    |

- Accessing by ID:
  We can access any HTML tag by providing it with an id and then using the below javascript code to access the tag by its id.
  ```
  const elem = document.getElementById(‘id’);
  ```
- Accessing by class name:
  We can also access any HTML tag by assigning a class to it and use the given javascript code to store it in a variable.
  ```
  const elem = document.getElementByClassName(‘classname’);
  ```
- Accessing by tag name
  We can directly access the tag using the below javascript code.
  ```
  document.getElementByTagName();
  ```
  ```
  const elem = document.getElementByTagName(‘h1’);
  ```
- Using query selector
  We can access single element or multiple elements of HTML file using query selector
  ```
  const singleQueryDemo = document.querySelector(‘#singleSelector’);
  const multipleQueryDemo = document.querySelectorAll(‘.  multipleSelector’);
  ```

## 5. Some Javascript Helper Methods for DOM

In order to perform the DOM manipulation, javascript provides some helper methods which make it easier to manipulate the nodes of the DOM tree as per our need. These methods allow adding, modify and delete the content or style of the HTML element. Some of the most commonly used helper methods are:

- innerHTML:
  This method is used to change HTML content of a tag
  ```
  document.getElementById(“#id).innerHTML = “This is DOM blog!”;
  ```
  We can also make tag nested by putting another tag inside a tag
  ```
  document.getElementsByTagName("div").innerHTML = "<p>Welcome to MountBlue</p>"
  ```
- .src :
  This method helps in changing the value of an attribute
  ```
  document.getElementsByTag(“img”).src = “dom.png”;
  ```
- createElement():
  This method helps in creating new element by taking the tag name as the parameter

  ```
  const div = document.createElement(‘div’);
  ```

- remove: This method is used to remove element from the tree it belongs to.<br>
  Consider this HMTL snippet
  ```
  <div id="id">This is a div</div>
  ```
  ```
  const div = document.getElementById('id');
  div.remove();
  ```
  <br>
  <br>
  These are some of the important javascript helper methods for DOM manipulation
  <br>

## 6. Conclusion

DOM is a very important and useful API which enables us to perform various operations. DOM manipulation is very important to make interactive web page. Some javascript libraries/frameworks use perform DOM manipulation in a very efficient way like React, jQuery, Angular,etc

## 7. References

- https://www.mozilla.org/en-US/firefox/browsers/what-is-a-browser/
- https://daoudisamir.com/document-object-model/
- https://www.digitalocean.com/community/tutorials/how-to-access-elements-in-the-dom
- https://www.html5rocks.com/en/tutorials/internals/howbrowserswork/#The_rendering_engine
- https://gabrieltanner.org/blog/javascript-dom-introduction
